#ifndef gamelogic_H
#define gamelogic_H

struct Vec3{
	float x,y,z;
        Vec3(){x=y=z=0;}
	Vec3(float a, float b, float c){x=a, y=b, z=c;}

	Vec3 operator - (Vec3& v){return Vec3(x-v.x, y-v.y, z-v.z);}
	Vec3 operator + (Vec3& v){return Vec3(x+v.x, y+v.y, z+v.z);}
	Vec3 operator * (double d){return Vec3(x*d, y*d, z*d);}
	Vec3 operator / (double d){return Vec3(x/d, y/d, z/d);}

	Vec3 normalize() {
		float len = sqrt(x*x+y*y+z*z);
		return Vec3(x/len, y/len, z/len);
	};
};
